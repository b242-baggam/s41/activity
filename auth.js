const jwt =require ("jsonwebtoken");
const secret = "courseBookingAPI";
//JSON Web Tokens
// Token Creation
module.exports.createAccessToken=(user)=>{
	const data = {
		id: user._id,
		email:user.email,
		isAdmin:user.isAdmin
	}
	return jwt.sign(data, secret, {});
}

// Token verification
module.exports.verify=(req, res, next)=>{
	//The token is retrieved from the request header
	let token= req.headers.authorization;

	if(typeof token !=="undefined"){
		console.log(token)

		token = token.slice(7, token.length);

		//Validate the token using the "verify" method decrypting the token using the secret code
		return jwt.verify(token, secret, (err, data)=>{
			//if jwt is not valid
			if(err){
				return res.send({auth : "failed" });
			}
			//if jwt is valid
			else{
				next();
			}
		})
	}
	//token does not exist
	else{
		return res.send({auth: "failed"});
	}
}

//Token decryption
module.exports.decode=(token)=>{
	//Token received and is no undefined
	if(typeof token !== "undefined"){
		//retrieves only the token and removes the "Bearer" prefix
		token=token.slice(7,token.length);
		return jwt.verify(token,secret, (err, data)=>{
			if (err){
				return null;
			}
			else{
				return jwt.decode(token, {complete:true}).payload
			}
		})
	}
	//token does not exist
	else{
		return null;
	}
}
